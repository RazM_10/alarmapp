package org.myself.alarmapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Vibrator;
import android.widget.Toast;

public class Alarm extends BroadcastReceiver {
    private Handler handler = new Handler();
    Context context;
    int count = 0;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        Toast.makeText(context, "Alarm", Toast.LENGTH_SHORT).show();
//        Vibrator v=(Vibrator)context.getSystemService(context.VIBRATOR_SERVICE);
//        v.vibrate(3000);
//        if(count<4){
        runnable.run();
//        }
//        else {
//            handler.removeCallbacks(runnable);
//        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Vibrator v = (Vibrator) context.getSystemService(context.VIBRATOR_SERVICE);
            v.vibrate(2000);
            count++;
            handler.postDelayed(this, 6000);
            if (count == 2) {
                stopRepeating();
            }
        }
    };

    private void stopRepeating(){
        handler.removeCallbacks(runnable);
        ((MainActivity) MainActivity.context).stopRepeating();
    }
}
