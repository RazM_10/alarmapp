package org.myself.alarmapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    EditText editTextTimeDate;
    TextView textViewTimeDate;
    Button buttonSetAlarm;

    int h, m;

    public AlarmManager alarmManager;
    public PendingIntent pendingIntent;

    public static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        listener();
    }

    public void init() {
        context=MainActivity.this;

        editTextTimeDate = findViewById(R.id.editTextTimeDate);
        textViewTimeDate = findViewById(R.id.textViewTimeDate);
        buttonSetAlarm = findViewById(R.id.buttonSetAlarm);

        Calendar calendar = Calendar.getInstance();
        h = calendar.get(Calendar.HOUR_OF_DAY);
        m = calendar.get(Calendar.MINUTE);

        editTextTimeDate.setText(+h + ":" + m);
    }

    public void listener() {
        buttonSetAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String time=editTextTimeDate.getText().toString();
                String[] timeArray=time.split(":");
                int hh= Integer.parseInt(timeArray[0]);
                int mm= Integer.parseInt(timeArray[1]);

                Calendar calendar=Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, hh);
                calendar.set(Calendar.MINUTE, mm);
                calendar.set(Calendar.SECOND, 0);

                Intent intent = new Intent(MainActivity.this, Alarm.class);
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
                alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pendingIntent);
            }
        });
    }
    public void stopRepeating(){
        alarmManager.cancel(pendingIntent);
        Toast.makeText(this, "Stop Alaem", Toast.LENGTH_SHORT).show();
    }
}
